locals {
  num_k3s_master = length(var.master_ips)
  num_k3s_worker = length(var.worker_ips)
}

resource "tls_private_key" "id_ecdsa_master" {
  algorithm   = "RSA"
  rsa_bits = "4096"
}

resource "tls_private_key" "id_ecdsa_worker" {
  algorithm   = "RSA"
  rsa_bits = "4096"
}

resource "local_sensitive_file" "sshkey_master" {
  content = tls_private_key.id_ecdsa_master.private_key_pem
  filename = join("/", [pathexpand("~/.ssh/"), "id_ecdsa_master"])
  file_permission = "0600"
}

resource "local_sensitive_file" "sshkey_worker" {
  content = tls_private_key.id_ecdsa_worker.private_key_pem
  filename = join("/", [pathexpand("~/.ssh/"), "id_ecdsa_worker"])
  file_permission = "0600"
}

resource "proxmox_vm_qemu" "proxmox_vm_master" {
  count       = local.num_k3s_master
  name        = "k3s-master-${count.index}"
  target_node = var.pm_node_name
  clone       = var.tamplate_vm_name
  os_type     = "cloud-init"
  agent       = 1
  memory      = var.num_k3s_masters_mem
  cores       = 4

  ipconfig0 = "ip=${var.master_ips[count.index]}/${var.networkrange},gw=${var.gateway}"
  
  ciuser = "ubuntu"
  sshkeys = tls_private_key.id_ecdsa_master.public_key_openssh
  
  lifecycle {
    ignore_changes = [
      disk,
      network
    ]
  }

}

resource "proxmox_vm_qemu" "proxmox_vm_workers" {
  count       = local.num_k3s_worker
  name        = "k3s-worker-${count.index}"
  target_node = var.pm_node_name
  clone       = var.tamplate_vm_name
  os_type     = "cloud-init"
  agent       = 1
  memory      = var.num_k3s_nodes_mem
  cores       = 4

  ipconfig0 = "ip=${var.worker_ips[count.index]}/${var.networkrange},gw=${var.gateway}"
  
  ciuser = "ubuntu"
  sshkeys = tls_private_key.id_ecdsa_worker.public_key_openssh

  lifecycle {
    ignore_changes = [
      disk,
      network
    ]
  }

}

data "template_file" "k8s" {
  template = file("./templates/k8s.tpl")
  vars = {
    k3s_master_ip = "${join("\n", [for instance in proxmox_vm_qemu.proxmox_vm_master : join("", [instance.default_ipv4_address, " ansible_ssh_private_key_file=", local_sensitive_file.sshkey_master.filename])])}"
    k3s_node_ip   = "${join("\n", [for instance in proxmox_vm_qemu.proxmox_vm_workers : join("", [instance.default_ipv4_address, " ansible_ssh_private_key_file=", local_sensitive_file.sshkey_worker.filename])])}"
  }
}

resource "local_file" "k8s_file" {
  content  = data.template_file.k8s.rendered
  filename = "../inventory/my-cluster/hosts.ini"
}

resource "local_file" "var_file" {
  source   = "../inventory/sample/group_vars/all.yml"
  filename = "../inventory/my-cluster/group_vars/all.yml"
}
