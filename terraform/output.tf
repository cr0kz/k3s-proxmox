output "master_ips" {
  value = ["${proxmox_vm_qemu.proxmox_vm_master.*.default_ipv4_address}"]
}

output "worker_ips" {
  value = ["${proxmox_vm_qemu.proxmox_vm_workers.*.default_ipv4_address}"]
}

output "master_key" {
  value = tls_private_key.id_ecdsa_master.private_key_pem
  sensitive = true
}

output "worker_key" {
  value = tls_private_key.id_ecdsa_worker.private_key_pem
  sensitive = true
}